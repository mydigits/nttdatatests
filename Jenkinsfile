pipeline {
    agent any
    options {
         timeout(time: 10, unit: 'MINUTES')
    }
    environment {
        DEPLOYMENT_STAGE_CI = sh(script: "echo $BRANCH_NAME | awk \'{split(\$0,a,\"/\"); print tolower(a[2])\"-ci\"}\'",
                                 returnStdout: true).trim()        
        AWS_REGION = 'eu-central-1'
        AWS_PROFILE = 'UCP_DEV'
        ACCOUNTNUMBER = '024861556389' 
        //Stack names may not exceed 17 chars. The name is hardly cut which leads to errors due to image not not found during container start
        STACK_NAME= "${'pdfs-' + env.GIT_COMMIT.substring(0,10) + '-' + (int) System.currentTimeMillis() % 10 }"
        SERVICE_BASE_URL='http://internal-ECSALB-544772805.eu-central-1.elb.amazonaws.com:8080/pdfs-service'
    }
    tools {
        maven 'apache-maven-3.3.9'
        jdk 'jdk1.8.0_121'
    }
    stages {
         stage("Prepare") {
            steps {
                    bitbucketStatusNotify buildState: "INPROGRESS"
                }
            }
        stage('CI Build') {
            steps {
                sh 'mvn clean install failsafe:integration-test failsafe:verify'
            }
        }
        stage('Sonar Quality Gate') {
           steps {
                withSonarQubeEnv('SonarQubeUCP') {
                    sh 'mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install -Dmaven.test.failure.ignore=true'
                    sh 'mvn sonar:sonar -Dsonar.host.url=https://ucp-build.bmwgroup.net:5000/newsonar -Dsonar.login=$SONAR_AUTH_TOKEN -Dsonar.branch=$BRANCH_NAME'
                }
                timeout(time: 1, unit: 'MINUTES') { // Just in case something goes wrong, pipeline will be killed after a timeout
                     // when sonarscannr plugin available use instead -> waitForQualityGate abortPipeline: true
                     script {
                         sleep 15   // This is needed to avoid a hickup with sonar webhook
                         def qg = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
                         echo 'Quality Gate Result ' + qg.status
                         //if (qg.status != 'OK') {
                         //    error "Pipeline aborted due to quality gate failure: ${qg.status}"
                         //}
                     }
                }
            }
        }
        stage('Create Test Image') {
            steps {
              sh '''#!/bin/bash
					aws cloudformation delete-stack  --stack-name ${STACK_NAME} --region ${AWS_REGION} --profile ${AWS_PROFILE}
				'''		
			  sh '''#!/bin/bash	
                    IMAGETAG="ecs-repository:${STACK_NAME}"
                    echo "stack name ${STACK_NAME}"	
                    echo "image tag ${IMAGETAG}"
					$(`aws ecr get-login --region ${AWS_REGION} --profile ${AWS_PROFILE}`)									
					echo 'building docker image'
					docker build  -f Dockerfile -t ${IMAGETAG} .
					docker tag ${IMAGETAG} ${ACCOUNTNUMBER}.dkr.ecr.eu-central-1.amazonaws.com/${IMAGETAG}				
					echo "push image ${IMAGETAG} to ecr named ecs-repository"
					docker push ${ACCOUNTNUMBER}.dkr.ecr.eu-central-1.amazonaws.com/${IMAGETAG}
				'''
            }
        }
        stage('Deploy Test Environment') {
            environment {
                  RULE_PRIO = "${(int) System.currentTimeMillis() % 50000}"
            }
            steps {
				sh '''#!/bin/bash
                    echo 'wait for stack removal'
					aws cloudformation wait stack-delete-complete --stack-name ${STACK_NAME} --region ${AWS_REGION} --profile ${AWS_PROFILE}
                    echo "create stack ${STACK_NAME}"
					aws cloudformation create-stack  --stack-name ${STACK_NAME} --template-body "file:///${WORKSPACE}/pdf-service-td.yml" --parameters ParameterKey=BranchName,ParameterValue=${STACK_NAME} ParameterKey=ALBListenerRulePriority,ParameterValue=${RULE_PRIO} --capabilities CAPABILITY_NAMED_IAM --region ${AWS_REGION} --profile ${AWS_PROFILE}
					echo 'wait for stack creation'
					aws cloudformation wait stack-create-complete --stack-name ${STACK_NAME} --region ${AWS_REGION} --profile ${AWS_PROFILE}
					echo "stack ${STACK_NAME} deployed to ECS"
				''' 
            }
        }
        stage('Execute System Tests') {
            steps {
				sh '''#!/bin/bash
                    echo 'Waiting for LB targets to become healthy!'
                    sleep 30
					echo 'Executing SystemTests against ECS Deployment!'
					mvn test -Dtest=*ST -Dit.test=*ST -DfailIfNoTests=false -DHOST=${SERVICE_BASE_URL} -DBRANCH_NAME=${STACK_NAME}.ucp -DSKIP_SONAR=TRUE  -DproxySet=true -DproxyHost=proxy.muc -DproxyPort=8080
				'''
            }
        }
        stage('Execute PO Tests') {
            steps {
                echo 'Hello world!'
            }
        }
    }
     post {
          always {
               echo 'Removing service deployment from ECS'
               build job: 'UCP_Buildchain_cleanup', parameters: [[$class: 'StringParameterValue', name: 'STACK_NAME', value: "${STACK_NAME}"]], wait: false
          }

          success {
              bitbucketStatusNotify buildState: "SUCCESSFUL"
          }

          failure {
              bitbucketStatusNotify buildState: "FAILED"
			  
			  mail to: "ucp.platform2@list.bmw.com, ucp.clients@list.bmw.com, ucp.base@list.bmw.com",
              subject: "Failed Pipeline: ${currentBuild.displayName}",
              body: "Something is wrong with Branch: - ${env.BRANCH_NAME} -"
			
          }
        }
}