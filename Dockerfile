FROM payara/server-full:174

ENV MICRO_SERVICE_NAME=pdfs PAYARA_DOMAIN=payaradomain \
    DOMAIN_INIT_COMMANDS=$PAYARA_PATH/create-ucp-standard-server.txt  \
    IS_DOCKER_ENV=TRUE \
    DOWNLOAD_ENV_FROM_S3=TRUE \
    MONITORING_FREQUENCY=300

COPY $MICRO_SERVICE_NAME-rest/target/dependency/postgresql-42.1.1.jar $PAYARA_PATH/glassfish/domains/$PAYARA_DOMAIN/lib/ext
COPY $MICRO_SERVICE_NAME-rest/create-ucp-standard-server.txt $PAYARA_PATH

COPY $MICRO_SERVICE_NAME-rest/target/$MICRO_SERVICE_NAME-service.war  $DEPLOY_DIR

COPY $MICRO_SERVICE_NAME-flyway-migration/target/dependency/flyway-commandline-4.1.2.tar.gz /usr/src/
COPY $MICRO_SERVICE_NAME-flyway-migration/src/main/resources/migrations/v3/$MICRO_SERVICE_NAME/postgresql /usr/src/flyway-migrations

RUN $PAYARA_PATH/glassfish/bin/asadmin start-domain ${PAYARA_DOMAIN} && \
    $PAYARA_PATH/glassfish/bin/asadmin --user admin --passwordfile /opt/pwdfile < $DOMAIN_INIT_COMMANDS && \
    $PAYARA_PATH/glassfish/bin/asadmin --user admin --passwordfile /opt/pwdfile \
    create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource \
    --restype javax.sql.ConnectionPoolDataSource --property "user=\${DATABASE_USER}:password=\${DATABASE_PASSWORD}:servername=\${DATABASE_HOST}:port=\${DATABASE_PORT}:databasename=\${DATABASE_NAME}" \
     jdbc/${MICRO_SERVICE_NAME}_pool && \
    $PAYARA_PATH/glassfish/bin/asadmin --user admin --passwordfile /opt/pwdfile \
    create-jdbc-resource --connectionpoolid jdbc/${MICRO_SERVICE_NAME}_pool jdbc/${MICRO_SERVICE_NAME} && \
    $PAYARA_PATH/glassfish/bin/asadmin --user admin --passwordfile /opt/pwdfile \
        set resources.jdbc-connection-pool.jdbc/${MICRO_SERVICE_NAME}_pool.validation-classname="org.glassfish.api.jdbc.validation.PostgresConnectionValidation" && \
    $PAYARA_PATH/glassfish/bin/asadmin --user admin --passwordfile /opt/pwdfile \
        set resources.jdbc-connection-pool.jdbc/${MICRO_SERVICE_NAME}_pool.connection-validation-method=custom-validation \
            resources.jdbc-connection-pool.jdbc/${MICRO_SERVICE_NAME}_pool.fail-all-connections=true \
            resources.jdbc-connection-pool.jdbc/${MICRO_SERVICE_NAME}_pool.statement-cache-type= \
            resources.jdbc-connection-pool.jdbc/${MICRO_SERVICE_NAME}_pool.is-connection-validation-required=true

COPY $MICRO_SERVICE_NAME-rest/start.sh  $PAYARA_PATH
COPY $MICRO_SERVICE_NAME-flyway-migration/migrate-db.sh  $PAYARA_PATH

USER root

RUN apt-get update -y  && \
    apt-get install -y jq dos2unix python python-dev python-pip && \
    pip install awscli && \
    dos2unix $PAYARA_PATH/start.sh && \
    dos2unix $PAYARA_PATH/migrate-db.sh && \
    chown payara:payara $PAYARA_PATH/start.sh && \
    chmod u+x $PAYARA_PATH/start.sh && \
    chown payara:payara $PAYARA_PATH/migrate-db.sh && \
    chmod u+x $PAYARA_PATH/migrate-db.sh && \
    cd /usr/src && tar -xf flyway-commandline-4.1.2.tar.gz && \
    chown -R payara:payara /usr/src/flyway-4.1.2 && \
    chown -R payara:payara /usr/src/flyway-migrations

USER payara

EXPOSE 8080

ENTRYPOINT ["./start.sh"]
